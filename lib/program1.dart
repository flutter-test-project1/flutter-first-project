import 'package:flutter/material.dart';

class Program1 extends StatelessWidget {
  const Program1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Enter Your Name",
                  ),
                ),

                TextFormField(
                  onChanged: (value) {},
                  obscureText: true,
                  obscuringCharacter: "*",
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Enter Your Name",
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }
}
