import 'package:flutter/material.dart';

class WhatsappMain extends StatelessWidget {
  List<Map<String, dynamic>> userList = [];

  void insertUser({
    required String userName,
    required String userImage,
    required String lastMessage,
    required int counter,
  }) {
    Map<String, dynamic> map = {};

    map['UserName'] = userName;
    map['UserImage'] = userImage;
    map['LastMessage'] = lastMessage;
    map['Counter'] = counter;

    userList.add(map);
  }

  @override
  Widget build(BuildContext context) {
    insertUser(
        userName: 'Khushi',
        userImage: 'images/pre_login_page.jpeg',
        lastMessage: 'Hello',
        counter: 5);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "WhatsApp",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 30,
          ),
        ),
        backgroundColor: Colors.green,
      ),
      body: ListView.builder(
        itemCount: userList.length,
        itemBuilder: (context, index) {
          return Card(
              elevation: 5,
              margin: EdgeInsets.all(10),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    CircleAvatar(
                      foregroundImage: AssetImage(userList[index]['UserImage']),
                      radius: 22,
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              userList[index]['UserName'],
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                              ),
                            ),
                            Text(
                              userList[index]['LastMessage'],
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 16,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.green,
                      ),
                      padding: EdgeInsets.all(6),
                      child: Text(
                        userList[index]['Counter'].toString(),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                    )
                  ],
                ),
              ));
        },
      ),
    );
  }
}
