import 'package:flutter/material.dart';
import 'package:fluttertestproject/home_page.dart';

class BigBasketLogin extends StatefulWidget {
  @override
  State<BigBasketLogin> createState() => _BigBasketLoginState();
}

class _BigBasketLoginState extends State<BigBasketLogin> {
  var userNameController = TextEditingController();
  var passwordController = TextEditingController();
  bool isVisible = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            fit: BoxFit.fitWidth,
            "assets/images/pre_login_page.jpg",
          ),
          Container(color: Color(0x33AAAAAA)),
          Column(
            children: [
              Expanded(
                flex: 1,
                child: Container(),
              ),
              Expanded(
                flex: 1,
                child: Container(color: Colors.grey[300]),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(right: 10, left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: CircleAvatar(
                    child: BackButton(),
                    backgroundColor: Colors.white60,
                  ),
                  alignment: AlignmentDirectional.topStart,
                ), //Back Button
                Container(
                  padding: const EdgeInsets.only(top: 40.0, bottom: 95),
                  child: Image.asset(
                    "images/logo1.png",
                    color: Colors.white,
                    height: 50,
                    width: 50,
                  ),
                ), //Image
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  child: Text(
                    "Login/Signup",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                    ),
                  ),
                ), //Text
                Container(
                  padding: EdgeInsets.only(
                    top: 60,
                    right: 10,
                    left: 10,
                    bottom: 5,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                    ),
                    color: Colors.white,
                  ),
                  child: TextFormField(
                    controller: userNameController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Enter User Name",
                      prefixIcon: Icon(
                        Icons.person,
                      ),
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(
                            () {
                                userNameController.clear();
                            },
                          );
                        },
                        icon: Icon(
                          Icons.highlight_remove,
                          size: 27,
                        ),
                      ),
                    ),
                  ),
                ), //User Name
                Container(
                  padding: EdgeInsets.only(right: 10, left: 10),
                  color: Colors.white,
                  child: TextFormField(
                    controller: passwordController,
                    obscureText: isVisible,
                    obscuringCharacter: "*",
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Enter Password",
                      prefixIcon: Icon(
                        Icons.lock_sharp,
                      ),
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(
                            () {
                              isVisible = !isVisible;
                            },
                          );
                        },
                        icon: Icon(
                          isVisible
                              ? Icons.visibility_outlined
                              : Icons.visibility_off_outlined,
                          size: 27,
                        ),
                      ),
                    ),
                  ),
                ), //Password
                Container(
                  padding: EdgeInsets.only(right: 10, left: 10),
                  height: 65,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(5),
                      bottomRight: Radius.circular(5),
                    ),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      Expanded(child: Container()),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (context) {
                                return HomePage();
                              },
                            ),
                          );
                        },
                        child: Text(
                          "Continue",
                          style: TextStyle(color: Colors.white),
                        ),
                        style: TextButton.styleFrom(
                          elevation: 7,
                          shadowColor: Colors.purple[900],
                          backgroundColor: Color(
                            0XFF311B92,
                          ),
                          shape: StadiumBorder(),
                        ),
                      ),
                      Expanded(child: Container()),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
